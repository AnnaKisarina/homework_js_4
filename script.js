function calculate (num1, num2, sign) {
    let result = null;
    switch (sign) {
        case '+':
            result = num1 + num2;
            break;
        case '-':
            result = num1 - num2;
            break;
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
        default: result = 'Error: unexpected operation';
    }
    return result;
}

let num1 = Number(prompt("Enter the first value"));
let num2 = Number(prompt("Enter the second value"));
let operation = prompt("Enter a math operation");


console.log(calculate(num1, num2, operation));